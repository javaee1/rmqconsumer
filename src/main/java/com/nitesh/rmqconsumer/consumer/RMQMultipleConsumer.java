package com.nitesh.rmqconsumer.consumer;

import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RMQMultipleConsumer {
	
	private final Logger log = LoggerFactory.getLogger(RMQMultipleConsumer.class);
	
	@RabbitListener(queues = "course.fixedrate", concurrency = "3")
	public void listen(String message) {
		
		log.info("consuming {}", message, Thread.currentThread().getName());
		
		try {
			Thread.sleep(ThreadLocalRandom.current().nextLong(2000));
		}
		catch(InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
