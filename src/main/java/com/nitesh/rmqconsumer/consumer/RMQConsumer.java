package com.nitesh.rmqconsumer.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RMQConsumer {
	
	@RabbitListener(queues="helloworld")
	public void listen(String message) {
		System.out.println("Consumer is running");
		System.out.println("Consuming "+ message);
	}

}
